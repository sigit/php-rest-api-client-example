<?php
/*
 * Insert Consumer example
 *
 */

require(__DIR__ . '/bootstrap.php');

$params = array(
  'first_name' => 'fulan',
  'last_name' => 'fulan',
  'gender' => 'f',
  'email' => 'fulan@fulan.com'
);

$result = $api->post("consumers/", $params);
if($result->info->http_code == 201){
    $data = json_decode($result->response);
    var_dump($data);
}
