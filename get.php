<?php
/*
 * Get List / Detail Data example
 *
 */

require(__DIR__ . '/bootstrap.php');

# list 
$result = $api->get("consumers/");
if($result->info->http_code == 200) {
    $data = json_decode($result->response);
    var_dump($data);
}

# detail
$result = $api->get("consumers/12345/");
if($result->info->http_code == 200){
    $data = json_decode($result->response);
    var_dump($data);
}
