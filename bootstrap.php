<?php

require(__DIR__ . '/restclient.php'); # https://github.com/tcdent/php-restclient

$url = 'SERVER_URL';
$token = "YOUR TOKEN HERE";

$api = new RestClient(array(
    'base_url' => $url, 
    'format' => "json",
    'headers' => array(
      'Authorization' => "Token $token",
      'accept' => 'application/json'
    ),
));

?>
